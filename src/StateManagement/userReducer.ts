import { User } from "../models/user";

interface State {
    user: User | null;
    list: User[];
}

export const Actions = Object.freeze({
    setUser: (user?: User) => ({ type: '[USER_STATE] USER_SET', payload: user })
});

const initialState: State = {
    user: null,
    list: []
}

const userReducer = (state = initialState, action: any) => {
    console.log('user',state);
    switch (action.type) {
        case'[USER_STATE] USER_SET':
            const localuser = action.payload;
            if(localuser){
                const localList = [...state.list];
                const index = localList.findIndex(x => x.username === localuser.username);
                if(index === -1){
                    localList.push(localuser);
                }
                const newState = { ...state, user: localuser, list: [...localList] };
                return newState;
            }
            else{
                return { ...state, user: localuser };
            }
        default:
            return state;
    }
};
export default userReducer;