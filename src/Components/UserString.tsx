import { User } from "../models/user";
import { connect, useSelector } from "react-redux";


interface Props {
    selectedUser?: User;
}

const UserString = (props: Props) => {
    const { selectedUser } = props;   
    const userFromGs = useSelector((gs:any) => gs.user.user);    
    const msgName = userFromGs ? 'Привет, ' + userFromGs.username : 'Необходимо войти или зарегистрироваться';

    return <div>
        <div>{msgName}</div>    
    </div>
}

// Функция возвращает объект с нужными нам данными
// Из редакс
// При этом поля в объекте
// Должны называться также
// как и пропсы,
// с которым мы его соединяем
const mapStateToProps = (globalState: any):Props => {

    return { selectedUser: globalState.user.user };
}


// При помощи connect
// Мы соединяем данные redux из mapStateToProps
// С пропсами компонента Thunk
export default connect(mapStateToProps)(UserString);