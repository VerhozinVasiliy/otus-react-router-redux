import { Actions } from "../../StateManagement/userReducer";
import React from 'react';
import store from '../../store';
import { User } from "../../models/user";

const withCommonAuth = (EnterComponent:any) => {
  class AuthHOC extends React.Component {

    render() {
      const user = store.getState().user.user;
      const userListFromGs = store.getState().user.list;

      const exit = () => {
        store.dispatch(Actions.setUser(undefined));
      };

      const select = (user:User) => {
        store.dispatch(Actions.setUser(user));
    };

      return (
        <div>
            <EnterComponent userLocal={user} onExit={exit} onSelect={select} userList={userListFromGs}/>
        </div>
      );
    }
  }
    
  return AuthHOC;
};

export default withCommonAuth;
