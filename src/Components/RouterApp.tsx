import {
    Route,
    Link,
    Routes,
    BrowserRouter
  } from "react-router-dom";
import { HomePage } from './Pages/HomePage';
import { Navigator } from './Navigator';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import ErrorPage from "./Pages/ErrorPage";
import LoginWithCommon from "./Pages/Login";
import RegWithCommon from "./Pages/Register";

export function RouterApp(){
    return (
        <BrowserRouter>
            <Navigator />
            <Routes>
                <Route path="login" element={<LoginWithCommon />} /> 
                <Route path="register" element={<RegWithCommon />} /> 
                <Route path="homepage" element={<HomePage />} /> 
                <Route path="*" element={<ErrorPage />} />
            </Routes>
        </BrowserRouter>
    )
}