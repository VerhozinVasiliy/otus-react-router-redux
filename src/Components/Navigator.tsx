import { Button } from "@mui/material";
import Nav from "react-bootstrap/esm/Nav";
import Navbar from "react-bootstrap/esm/Navbar";
import { Link } from "react-router-dom";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

export function Navigator(){
    return (
        <Navbar bg="primary" data-bs-theme="dark">
            <Nav className="me-auto">
              <Button component={Link} to="/homepage" variant="contained">Главная страница</Button>
              <Button component={Link} to="/login" variant="contained">Войти</Button>
              <Button component={Link} to="/register" variant="contained">Зарегистрироваться</Button>
            </Nav>
        </Navbar>
    );
}