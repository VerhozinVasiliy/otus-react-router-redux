import UserString from "../UserString";
import { User } from "../../models/user";
import Button from '@mui/material/Button';
import { TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";
import AuthHOC from "../HOC/AuthHOC";

interface Props{
    userLocal: User,
    onExit: () => void,
    onSelect: (user:User) => void,
    userList: User[]
}

const loginFunc = function Login (props:Props) {
    let userName = '';
    let userPassword = '';

    console.log(props.userLocal ? 'user from hoc ' + props.userLocal.username : 'user undefined');
    const navigate = useNavigate();

    const onLocalExit = () => {
        props.onExit();
        navigate('/homepage');
    }
    
    const select = () => {
        const index = props.userList.findIndex((x:User) => x.username === userName && x.password === userPassword);
        if(index === -1){
            alert("Вам нужно зарегистрироваться");
            navigate('/register');
        }
        else{
            const localUser = props.userList[index];
            props.onSelect(localUser);
            navigate('/homepage');
        }
    };

    const setUserName = (e: any) => {
        userName = e.target.value;
    }

    const setPassword = (e: any) => {
        userPassword = e.target.value;
    }

    if(props.userLocal){
        return <div>
            <h1>Страница входа</h1>
            <UserString />           
            <Button variant="outlined" color="secondary" onClick={onLocalExit}>Выйти</Button>
        </div> 
    }
    else{
        return <div>
        <form autoComplete="off" onSubmit={select}>
                <h3>Страница входа</h3>
                <TextField 
                    label="Имя пользователя:"
                    onChange={ setUserName }
                    required
                    variant="outlined"
                    color="secondary"
                    sx={{mb: 3}}
                    fullWidth
                 />
                 <TextField 
                    label="Пароль"
                    onChange={ setPassword }
                    required
                    variant="outlined"
                    color="secondary"
                    type="password"
                    fullWidth
                    sx={{mb: 3}}
                 />
                 <Button variant="outlined" color="secondary" type="submit">Войти</Button>
        </form>
        </div>
    }
}

 const LoginWithCommon = AuthHOC(loginFunc);

 export default LoginWithCommon;


