import { Button, TextField } from "@mui/material";
import UserString from "../UserString";
import { useNavigate } from "react-router-dom";
import { User } from "../../models/user";
import AuthHOC from "../HOC/AuthHOC";

interface Props{
    userLocal: User,
    onExit: () => void,
    onSelect: (user:User) => void,
    userList: User[]
}

const regFunc =  function Register(props:Props) {
    console.log(props.userLocal ? 'user from hoc ' + props.userLocal.username : 'user undefined');
    let userName = '';
    let userPassword = '';
    let userEmail = '';
    const navigate = useNavigate();
    
    const onLocalExit = () => {
        props.onExit();
        navigate('/homepage');
    }
    
    const select = () => {
        const localUser = { username: userName, password: userPassword, email: userEmail }
        props.onSelect(localUser);
        navigate('/homepage');
    };

    const setUserName = (e: any) => {
        userName = e.target.value;
    }
    const setUserPassword = (e: any) => {
        userPassword = e.target.value;
    }
    const setEmail = (e: any) => {
        userEmail = e.target.value;
    }
    
    if(props.userLocal){
        return <div>
            <h1>Страница регистрации</h1>
            <UserString />
            <Button variant="outlined" color="secondary" onClick={onLocalExit}>Выйти</Button>
        </div> 
    }
    else{
        return <div>
            <form autoComplete="off" onSubmit={select}>
                <h3>Страница регистрации</h3>
                <TextField 
                    label="Имя пользователя:"
                    onChange={ setUserName }
                    required
                    variant="outlined"
                    color="secondary"
                    sx={{mb: 3}}
                    fullWidth
                 />
                 <TextField 
                    label="Пароль"
                    onChange={ setUserPassword }
                    required
                    variant="outlined"
                    color="secondary"
                    type="password"
                    fullWidth
                    sx={{mb: 3}}
                 />
                 <TextField 
                    label="E-mail"
                    onChange={ setEmail }
                    required
                    variant="outlined"
                    color="secondary"
                    type="email"
                    fullWidth
                    sx={{mb: 3}}
                 />
                 <Button variant="outlined" color="secondary" type="submit">Зарегистрироваться</Button>
        </form>
        </div>
    }
}

const RegWithCommon = AuthHOC(regFunc);

export default RegWithCommon;