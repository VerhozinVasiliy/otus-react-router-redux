import { configureStore } from '@reduxjs/toolkit';
import userReducer from './StateManagement/userReducer';

const store = configureStore({
    reducer: {
        // И под выбранного пользователя
        user: userReducer
    },

});
export default store;